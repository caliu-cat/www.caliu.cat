+++
title = "Organització"
date = "2021-10-17"
+++

Tal i com es defineix en els [estatuts](https://ftp.caliu.cat/pub/caliu/estatuts.pdf) de l'associació, els òrgans de govern d'aquesta són:

## L'Assemblea General

Integrada per tots els socis, es reuneix com a mínim una vegada a l'any i, entre d'altres coses, aprova si procedeix els comptes de l'associació, les possibles modificacions dels estatuts, i elegeix els membres de la Junta Directiva.

## La Junta Directiva

Òrgan de govern elegit per l'Assemblea General i que consta com a mínim de President, Secretari, Tresorer i vocal. La primera Junta directiva fou formada per en Francesc Genové en el càrrec de President, l'Eduard Fabra de Vicepresident, en Josep Ma Fàbrega de Secretari, l'Emili Colomer de Tresorer, i com a vocals, en Rafael Carreras, Xavier Drudis, Sergi Fernández, Leopold Palomo, Lluís Pérez i Miquel Chicano.

Des de juny de 2020, la junta està formada per:
* [Aniol Martí](mailto:president_a_caliu.cat), president [[2A34 1FB6 6014 06C1 44AD EEE4 C4C3 F43E C6F0 514F](http://pgp.surfnet.nl/pks/lookup?search=0xC6F0514F)]
* Orestes Mas, vicepresident
* [Rafael Carreras](mailto:secretari_a_caliu.cat), secretari
* [Josep Maria Ferrer](mailto:tresorer_a_caliu.cat), tresorer [[0255 AFCA CC35 57A9 4C40 387A EEFF 2370 A3F3 38D1](http://pgp.surfnet.nl/pks/lookup?search=0xA3F338D1)]
* Francesc Vilaubí, vocal [[AC0C FEB3 BDE4 5635 3EE0 97B1 27D8 6478 2C2C 618C](http://pgp.surfnet.nl/pks/lookup?search=0x2C2C618C)]
* David Pinilla, vocal
* Miquel Adroer, vocal
