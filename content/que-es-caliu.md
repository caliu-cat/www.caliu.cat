+++
title = "Associació d’usuaris/usuàries de GNU/Linux en llengua catalana"
date = "2021-10-17"
+++

Associació inscrita al [Registre Oficial d’Associacions de la Generalitat de Catalunya](http://www10.gencat.net/pls/dji_gedj/p01.mostrar_dades?v_tipent=2&amp;v_compent=47581) amb el número 25400 de la secció 1a del registre de Barcelona. El seu nom prové de les sigles en anglès de **Ca**talan **Li**nux **U**sers.

## Orígens de Caliu

Caliu va néixer el **24 de febrer de 1999** arran del missatge que l’Eduard Fabra va enviar al fòrum de GNU/Linux de [VilaWeb](http://www.vilaweb.com/) proposant la creació d’un grup d’usuaris de GNU/Linux en català. La secció d’informàtica de [VilaWeb](http://www.vilaweb.com/informatica/) publicà la notícia en l’edició del dia 1 de març de 1999:

> **Caliu català amb Linux**. Des del fòrum de Linux de VilaWeb Informàtica, l’Eduard Fabra llença una proposta que pot interessar força el personal que, als territoris de parla catalana, utilitza el sistema operatiu GNU/Linux: crear “*un grup d’usuaris de Linux en català per tal d’unificar recursos i esforços en l’adaptació al nostre idioma d’aquest magnífic sistema operatiu*“. Si us engresca la idea d’un grup de Catalan Linux Users (CALIU?) o bé si ja en coneixeu algun i el voleu presentar, no dubteu a continuar el debat al fòrum de VilaWeb Informàtica.

Les respostes i mostres de suport no trigaren en arribar, així **naixia el primer grup d’usuaris de GNU/Linux de parla catalana**. Mica en mica, el grup va guanyar en popularitat i es va anar dotant d’eines de comunicació com la pàgina web i llistes de correu. Més tard, casualment es va descobrir una altra iniciativa per defensar els interessos dels usuaris de GNU/Linux en Català, una plataforma denominada AULLUC (Associació d’Usuaris de Linux en LlengUa Catalana), promoguda per en Francesc Genové i d’altres integrants que estaven a punt de presentar els papers per a convertir-se legalment en associació. De les converses entre aquesta plataforma i el nostre grup es va decidir unificar esforços i crear el que avui coneixem com a CALIU: Associació d’Usuaris de GNU/Linux en Llengua Catalana.

Al llarg de tots aquests anys, hem passat de tenir allotjada la pàgina en un servidor de pàgines personals a tenir les nostres pròpies màquines. La primera màquina (Carbassa 1) la vàrem muntar de la ferralla informàtica que entre tots vàrem poder reunir, la segona màquina (Carbassa 2) i la màquina actual (Stallman), -aconseguides gràcies a les donacions- donen servei d’ftp a més de 150 persones diariàment i estan equipades amb més d’un *Terabyte* de disc dur. Tot això no seria possible sense la inestimable col·laboració de la [UPC](http://www.upc.edu/) que és qui ens allotja les màquines i ens proporciona l’ample de banda.

## Objectius de Caliu

* Promoure l’ús del sistema operatiu GNU/Linux.
* Facilitar ajuda i suport tècnic.
* Fomentar la instal·lació del sistema operatiu GNU/Linux en les administracions, empreses, universitats, instituts, escoles i l’usuari final.
* Traduir els documents de configuració i aplicacions més utilitzades al català per tal de facilitar l’ús del GNU/Linux.
* Donar a conèixer i instal·lar GNU/Linux mitjançant les reunions dels membres del grup i les anomenades festes d’instal·lació.
* Donar suport als fabricants i distribuïdors d’ordinadors que tinguin GNU/Linux preinstal·lat com una opció en els seus equips.
* Promoure la traducció de distribucions al català.

## Projectes en curs

* Organitzadors del Dia de la Llibertat del Programari a Barcelona des de l’any 2005.
* Organitzadors del Dia de la Llibertat del Maquinari a Barcelona des de l’any 2015.
* Allotgem diversos <a href="/servidor-de-ftp/">miralls de distribucions de GNU/Linux</a> al nostre servidor.
