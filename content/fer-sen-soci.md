+++
title = "Fer-se’n soci"
date = "2021-10-17"
+++

Com probablement ja coneixereu, l’objectiu principal de la nostra associació és la difusió i promoció del Programari Lliure. En aquest sentit Caliu&nbsp; duu a terme una sèrie d’activitats que van des dels serveis informàtics fins a l’activisme social, i entre les que podem destacar les següents:

* Jornada de difusió del Programari Lliure coincident amb el Dia de la Llibertat del Programari (DLP-SFD).
* Jornada de difusió del Programari Lliure coincident amb el Dia de la Llibertat del Maquinari (DLM-HFD).
* Cessió d’infraestructures a [l’equip de traducció al català de KDE](http://cat.kde.org/ "kdecat").
* Fundació i cessió d’infraestructures a [l’equip d’Ubuntu en català](http://ubuntu.cat/).
* Caliu ha tingut un paper molt destacat en la campanya europea contra les patents de programari.
* Manteniment d’un [servidor FTP](http://ftp.caliu.cat/ "FTP") que actua de mirall de les principals distribucions, i de repositori d’imatges ISO de distribucions menys conegudes.
* Suport als usuaris de GNU/Linux en particular, i Programari Lliure en general.

Comprensiblement, alguns d’aquests serveis com ara els patrocinis o el manteniment de la infrastructura informàtica requereixen ingressos. Per tal d’assegurar la futura continuïtat dels serveis i de la pròpia associació, fa ja algun temps que es va decidir començar a acceptar contribucions econòmiques dels seus socis i simpatitzants.

Per tant, si valoreu la tasca que fa Caliu i voleu que aquesta tingui continuïtat en el futur us convidem a donar suport a l’associació tot satisfent una quota periòdica de soci, en funció de quina sigui la vostra situació actual. Les quotes són de 30 € anuals, excepte per als estudiants i jubilats/aturats, que són de 10 i 20€ respectivament. A banda dels serveis esmentats anteriorment no us oferim gaire més, això és veritat, però O’Reilly ofereix els socis de Caliu descomptes de fins a un 35%, ja que formem part del seu [programa de grups d’usuaris](http://ug.oreilly.com/).

Si us en voleu fer soci, si us plau, contacteu amb la [Junta](mailto:junta_a_caliu.cat "correu de la junta"). Si sou menors d’edat, a més a més, haureu d’omplir [aquest formulari](https://ftp.caliu.cat/pub/caliu/AutoritzacioMenors.pdf) i enviar-lo a la Junta.
