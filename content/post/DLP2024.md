+++
author = "Rafael Carreras"
title = "Dia de la Llibertat del Programari 2024"
date = "2024-07-15"
tags = [
    "esdeveniments",
]
+++

Caliu celebra el Dia de la Llibertat del Programari el dissabte 21 de setembre a l'espai jove Bocanord de Barcelona amb xerrades relacionades amb el programari lliure.

Dades de l'esdeveniment:
Lloc: Espai Jove BocaNord
c/ Agudells, 37-45
08032 – Barcelona
T. 93 4299369
Metro Línia 5 El Carmel (accessos adaptats)
Autobús: V21, 39, 19, 86, 87 Nocturn: N4
Data: 21 de setembre
Xerrades:

| Hora | Títol     | Ponent |
|------| ----------- | ----------- |
| 16:30 | Llibres de cultura lliure | Rita @titi@bcn.fedi.cat |
| 17:30 | Muntatge i configuració del nou servidor de Caliu | Aniol Martí |
| 18:30 | Debat sobre l’estat actual del programari i xarxes lliures  | Caliu |
